package project.panel;

import javafx.event.EventHandler;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import project.common.Styles;
import project.components.*;
import project.database.*;
import project.log.Log;

import java.sql.Connection;

public class LoginPanel extends VBox {
    Connection connection = Database.getConnection();

    Admin admin = Admin.getInstance();

    private Session session = Session.getInstance();
    private Stage stage;
    private MainTextField adminField;
    private MainPasswordField passwordField;
    private MainBtn cancel, login;

    public LoginPanel(Stage stage) {
        super();
        super.setPadding(new Insets(8, 16, 8, 16));

        KeyHandler key = new KeyHandler();

        adminField = new MainTextField("Border");
        adminField.floatingTextProperty().set("Enter a Admin name");
        adminField.setOnKeyPressed(key);

        passwordField = new MainPasswordField("border");
        passwordField.floatingTextProperty().set("Enter Password");
        passwordField.setOnKeyPressed(key);

        this.stage = stage;

        this.init();
    }

    private HBox getBtnLayout() {
        login = new MainBtn("Login");
        login.setOnAction(event -> doLogin());
        login.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
        login.setBgColor(Styles.SUCCESS_COLOR.get());
        login.setTextColor("#FFFFFF");

        cancel = new MainBtn("Cancel");
        cancel.setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
        cancel.setBgColor(Styles.DANGER_COLOR.get());
        cancel.setTextColor("#FFFFFF");
        cancel.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setResizable(false);
            alert.initOwner(stage);
            alert.setContentText("Are you sure you want to Exit?");
            alert.setHeaderText("Exit?");
            alert.setTitle("Confirmation");
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK)
                    stage.close();
                else
                    alert.close();
            });
        });

        HBox box = new HBox(8);
        box.setPadding(new Insets(4, 2, 8, 2));
        box.getChildren().addAll(login, cancel);

        return box;
    }

    private void success() {
        stage.setTitle("Login Dashboard");
        stage.requestFocus();
        stage.resizableProperty().set(false);

        this.getChildren().addAll(new LoginGrid(), this.getBtnLayout());
    }

    private void failed() {
        Label label = new Label("Failed to connect to the database.\nTry another time.");
        label.setStyle(Styles.HEADER2.get());

        MainBtn btn = new MainBtn("Exit");
        btn.setOnAction(event -> stage.close());
        btn.setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
        btn.setBgColor(Styles.DANGER_COLOR.get());
        btn.setTextColor("#FFFFFF");

        VBox.setVgrow(this, Priority.ALWAYS);
        this.setSpacing(16);
        this.getChildren().addAll(label, btn);

        stage.setTitle("Failed");
        stage.requestFocus();
        stage.resizableProperty().set(false);
    }

    private void doLogin() {
        String name = adminField.getText();
        String password = passwordField.getText();

        if (admin.setLogin(name, password)) {
            System.out.println(name + "\t" + password);
            adminField.setText("");
            passwordField.setText("");
            Log.getInstance().setLog("Logged in by:" + name + " ID:" + admin.getId());
            new Dashboard(stage);
        } else {
            Log.getInstance().setLog("Failed to Logged in");
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setResizable(false);
            alert.initOwner(stage);
            alert.setContentText("Error admin name or password is incorrect.");
            alert.setHeaderText("Error");
            alert.setTitle("Warning");
            alert.showAndWait();
        }
        if (session.isNull())
            session.setSection(admin.getId());
        else
            session.updateSection(admin.getId());
    }

    private void init() {
        if (connection != null) {
            success();
            if (session.isSetSession()) {
                if (admin.setLogin(session.getName(), session.getPassword())) {
                    Log.getInstance().setLog("Logged in using session " + session.getName() +
                                             " ID:" + session.getId());
                    new Dashboard(stage);
                }
            }
        } else {
            Log.getInstance().setLog("Failed to Conntected to Databased");
            failed();
        }
    }

    private class LoginGrid extends GridPane {

        public LoginGrid() {
            super();
            super.setVgap(8);
            super.setHgap(4);

            init();
        }

        private void init() {
            this.setPadding(new Insets(8, 4, 10, 4));
            this.setAlignment(Pos.CENTER);
            this.add(adminField, 0, 0);
            this.add(passwordField, 0, 1);
        }
    }

    private class KeyHandler implements EventHandler<KeyEvent> {

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.ENTER) {
                doLogin();
            } else if (event.getCode() == KeyCode.ESCAPE) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setResizable(false);
                alert.initOwner(stage);
                alert.setContentText("Are you sure you want to Exit?");
                alert.setHeaderText("Exit?");
                alert.setTitle("Confirmation");

                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                        stage.close();
                    } else {
                        alert.close();
                    }
                });
            }
        }
    }
}
