package project.panel;

import io.github.palexdev.materialfx.css.themes.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import project.common.*;
import project.components.*;
import project.database.*;
import project.log.Log;
import project.modules.*;

import java.util.*;

public class Dashboard extends BorderPane {
    public VBox mainBox;

    Stage stage, oldStage;

    public Dashboard(Stage oldStage) {
        super();

        stage = new Stage();

        mainBox = new VBox(16);

        Scene scene = new Scene(this, 1050, 600, true, SceneAntialiasing.BALANCED);

        this.init();
        scene.getStylesheets().clear();

        MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

        stage.requestFocus();
        stage.centerOnScreen();
        stage.sizeToScene();
        stage.setTitle("Dashboard");
        stage.setScene(scene);
        stage.setOnCloseRequest(event -> System.exit(0));
        stage.show();

        this.oldStage = oldStage;
        oldStage.hide();
    }

    private void init() {
        Container center = new Container();
        Sidebar sidebar = new Sidebar();

        BorderPane.setMargin(center, new Insets(4, 8, 4, 16));
        BorderPane.setMargin(sidebar, new Insets(2, 1, 4, 1));

        this.setCenter(center);
        this.setLeft(sidebar);
    }

    private class Container extends VBox {
        public Container() {
            super(8);
            super.setPadding(new Insets(8));

            this.initialization();
        }

        private void initialization() {
            mainBox.getChildren().clear();
            mainBox.getChildren().add(new MainModel());

            this.getChildren().add(mainBox);
        }
    }

    private class Sidebar extends VBox {
        public Sidebar() {
            super(8);
            super.setStyle("-fx-background-color: #fafafa;"
                           + "-fx-background-insets: -1 -5 -1 -1;"
                           + "-fx-background-radius: 0 16 8 0;"
                           + "-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.3), 16, 0.05, 4, 8);");

            // super.setPrefHeight(stage.getMaxHeight());
            // super.prefHeightProperty().bind(stage.heightProperty());
            super.setPrefWidth(230);
            super.setPadding(new Insets(16, 10, 10, 8));
            super.setAlignment(Pos.TOP_CENTER);

            VBox.setVgrow(this, Priority.ALWAYS);
            this.init();
        }

        private void init() {
            Label label = new Label("Sidebar");
            label.setStyle(Styles.HEADER1.get());

            this.getChildren().add(label);

            List<MainBtn> btn_list = new ArrayList<>();

            for (int i = 0; i < 6; i++) {
                btn_list.add(getBtn());
                this.getChildren().add(btn_list.get(i));
            }

            Label dashboard_icon = new ImgItem("src/main/resources/images/icon/monitor.png").getIcon();
            dashboard_icon.setAlignment(Pos.CENTER);

            Label books_icon = new ImgItem("src/main/resources/images/icon/books_collection.png").getIcon();
            books_icon.setAlignment(Pos.CENTER);

            Label users_icon = new ImgItem("src/main/resources/images/icon/user.png").getIcon();
            users_icon.setAlignment(Pos.CENTER);

            Label issue_icon = new ImgItem("src/main/resources/images/icon/quality.png").getIcon();
            issue_icon.setAlignment(Pos.CENTER);

            Label setting_icon = new ImgItem("src/main/resources/images/icon/settings.png").getIcon();
            setting_icon.setAlignment(Pos.CENTER);

            btn_list.get(0).setText("Dashboard");
            btn_list.get(0).setGraphic(dashboard_icon);
            btn_list.get(0).setGraphicTextGap(12);

            btn_list.get(1).setText("Books");
            btn_list.get(1).setGraphic(books_icon);
            btn_list.get(1).setGraphicTextGap(12);

            btn_list.get(2).setText("Users");
            btn_list.get(2).setGraphic(users_icon);
            btn_list.get(2).setGraphicTextGap(12);

            btn_list.get(3).setText("Approvals");
            btn_list.get(3).setGraphic(issue_icon);
            btn_list.get(3).setGraphicTextGap(12);

            btn_list.get(4).setText("Settings");
            btn_list.get(4).setGraphic(setting_icon);
            btn_list.get(4).setGraphicTextGap(12);

            btn_list.get(5).setText("Logout");
            btn_list.get(5).setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
            btn_list.get(5).setStyle("-fx-font-weight: bold;"
                                     + " -fx-font-size:16px;"
                                     + "-fx-text-fill:#FFF;"
                                     + "-fx-background-color:" + Styles.DANGER_COLOR.get());
            btn_list.get(5).setGraphic(new ImgItem("src/main/resources/images/icon/logout.png").getIcon());
            btn_list.get(5).setGraphicTextGap(12);
            btn_list.get(5).setOnAction(event -> {
                int id = Admin.getInstance().getId();
                Session session = Session.getInstance();

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setResizable(false);
                alert.initOwner(stage);
                alert.setContentText("Are you sure you want to logout");
                alert.setHeaderText("Logout");
                alert.setTitle("Confirmation");
                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                        Log.getInstance().setLog("Logged out; Id:" + id);
                        session.setLogout(id);
                        oldStage.show();
                        stage.close();
                    } else
                        alert.close();
                });
            });

            new SwitchNode(mainBox, btn_list.get(0)).switchNode(new MainModel());
            new SwitchNode(mainBox, btn_list.get(1)).switchNode(new BookModel());
            new SwitchNode(mainBox, btn_list.get(2)).switchNode(new UserModel());
            new SwitchNode(mainBox, btn_list.get(3)).switchNode(new ApprovalModel());
            new SwitchNode(mainBox, btn_list.get(4)).switchNode(new SettingModel());
        }

        private MainBtn getBtn() {
            MainBtn btn = new MainBtn();
            btn.setRippleColor(Color.web(Styles.INFO_ALT_COLOR.get()));
            btn.setStyle("-fx-font-weight: bold;"
                         + " -fx-font-size:16px;"
                         + "-fx-text-fill:#FFF;"
                         + "-fx-background-color:" + Styles.INFO_COLOR.get());
            btn.setNoDepthlevel();
            btn.setPadding(new Insets(16, 8, 16, 8));
            btn.setMinWidth(180);

            return btn;
        }
    }
}
