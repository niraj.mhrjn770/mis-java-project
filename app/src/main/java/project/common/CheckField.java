package project.common;

public class CheckField {
    private CheckField() {
    }

    public static boolean checkNumber(String text) {
        return text.matches(".*\\d.*");
    }
}
