package project.common;

import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class SwitchNode {
    private final Pane ROOT;
    private final Button BUTTON;

    public SwitchNode(Pane root, Button button) {
        this.ROOT = root;
        this.BUTTON = button;
    }

    public SwitchNode(Button button, Pane root) {
        this.ROOT = root;
        this.BUTTON = button;
    }

    public void switchNode(Pane node) {
        this.BUTTON.setOnAction(event -> switchLayout(node));
    }

    private void switchLayout(Pane node) {
        this.ROOT.getChildren().clear();
        this.ROOT.getChildren().add(node);
    }
}
