package project.common;

public enum Styles {
    HEADER1("-fx-font-size: 24;" + "-fx-text-fill:#484b6a;-fx-font-weight:bold;"),

    HEADER2("-fx-font-size: 18;" + "-fx-text-fill:#484b6a;-fx-font-weight:bold;"),

    DANGER_COLOR("#f53b3b"),

    DANGER_ALT_COLOR("#d21d1d"),

    SUCCESS_COLOR("#54B435"),

    SUCCESS_ALT_COLOR("#82CD47"),

    INFO_COLOR("#2fd1f7"),

    INFO_ALT_COLOR("#a6dfff");

    public final String label;

    private Styles(String label) {
        this.label = label;
    }

    public String get() {
        return label;
    }

}
