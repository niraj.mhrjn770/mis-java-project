package project.common;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import project.components.*;

public class CommonButton {
    private final static CommonButton instance = new CommonButton();

    private CommonButton() {
    }

    public static CommonButton getInstance() {
        return instance;
    }

    public class Delete extends MainBtn {
        public Delete() {
            super("Delete");
            this.init();
        }

        private void init() {
            Label icon = new ImgItem("src/main/resources/images/icon/dustbin.png").getIcon();

            this.setStyle("-fx-text-fill:#FFF;"
                    + "-fx-background-color:" + Styles.DANGER_COLOR.get());
            this.setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
            this.setGraphic(icon);
            this.setAlignment(Pos.CENTER);
            this.setGraphicTextGap(8);
        }

    }

    public class Refresh extends MainBtn {
        public Refresh() {
            super("Refresh");

            this.init();
        }

        private void init() {
            Label icon = new ImgItem("src/main/resources/images/icon/refresh.png").getIcon();

            this.setStyle("-fx-text-fill:#FFF;"
                    + "-fx-background-color:" + Styles.INFO_COLOR.get());
            this.setRippleColor(Color.web(Styles.INFO_ALT_COLOR.get()));
            this.setGraphic(icon);
            this.setAlignment(Pos.CENTER);
            this.setGraphicTextGap(8);
        }
    }

    public class Add extends MainBtn {
        public Add() {
            super("Add");
            this.init();
        }

        private void init() {
            Label icon = new ImgItem("src/main/resources/images/icon/add.png").getIcon();

            this.setStyle("-fx-text-fill:#FFF;"
                    + "-fx-background-color:" + Styles.SUCCESS_COLOR.get());
            this.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
            this.setGraphic(icon);
            this.setAlignment(Pos.CENTER);
            this.setGraphicTextGap(8);
        }
    }

    public class Update extends MainBtn {
        public Update() {
            super("Update");
            this.init();
        }

        public void setImg(String url) {
            Label icon = new ImgItem(url).getIcon();
            this.setGraphic(icon);
            this.setGraphicTextGap(8);
        }

        private void init() {
            this.setStyle("-fx-text-fill:#FFF;"
                    + "-fx-background-color:#1f9eff");
            this.setRippleColor(Color.web("#1fc3ff"));
            this.setAlignment(Pos.CENTER);
        }
    }
}
