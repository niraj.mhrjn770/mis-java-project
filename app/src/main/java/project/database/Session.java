package project.database;

import java.sql.*;

public class Session {
    private static Session instance;
    final private Connection conn;
    String admin, password;

    private Session() {
        conn = Database.getConnection();
    }

    public static Session getInstance() {
        if (instance == null)
            instance = new Session();

        return instance;
    }

    public void setSession(String admin, String password) {
        this.admin = admin;
        this.password = password;
    }

    public boolean isSetSession() {
        try (Statement statement = conn.createStatement()) {
            String sql = "Select admin_id from session where term=1";
            ResultSet rs = statement.executeQuery(sql);
            return rs.next();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean isNull() {
        int admin_id = Admin.getInstance().getId();
        System.out.println("admin_id = " + admin_id);
        try (Statement statement = conn.createStatement()) {
            String sql = "SELECT COUNT(*) FROM session where admin_id=" + admin_id;
            ResultSet rs = statement.executeQuery(sql);
            rs.next();
            int rowCount = rs.getInt(1);
            if (rowCount == 0) {
                System.out.println("session is empty");
                return true;
            } else {
                System.out.println("session is not empty");
                return false;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }


    public String getName() {
        try (Statement statement = conn.createStatement()) {
            int id = this.getId();
            String sql = "Select name from admin where id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            String name = "";
            while (rs.next()) {
                name = rs.getString("name");
            }
            return name;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String getPassword() {
        try (Statement statement = conn.createStatement()) {
            int id = this.getId();
            String sql = "Select password from admin where id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            String password = "";
            while (rs.next()) {
                password = rs.getString("password");
            }
            return password;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public int getId() {
        int id = -1;
        String sql = "Select admin_id from session where term=1";
        try (Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                id = rs.getInt("admin_id");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(id);
        }
        return id;
    }

    public void setSection(int admin_id) {
        String sql = "INSERT INTO session(term,admin_id) values (?,?)";
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, 1);
            statement.setInt(2, admin_id);
            statement.executeUpdate();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void updateSection(int admin_id) {
        String sql = "UPDATE session set term=1 where admin_id= " + admin_id;
        try {
            Statement statement = conn.createStatement();
            statement.execute(sql);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public void setLogout(int admin_id) {
        String sql = "UPDATE session set term=0 where admin_id= " + admin_id;
        try {
            Statement statement = conn.createStatement();
            statement.execute(sql);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

}
