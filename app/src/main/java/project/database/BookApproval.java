package project.database;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BookApproval {
    private final Connection conn;

    public BookApproval() {
        conn = Database.getConnection();
    }

    public ResultSet getBookApprovals(int bookID) {
        try {
            Statement statement = conn.createStatement();
            // return statement.executeQuery("SELECT * FROM `book_approval_request` where
            // book_id = " + bookID);
            return statement.executeQuery(
                    "SELECT * FROM `book_approval_request` inner join `user` on  book_approval_request.u_id = user.u_id where book_id = "
                            + bookID);
        } catch (Exception e) {
            System.err.println("UserApproval.getDetails: " + e.getMessage());
            return null;
        }
    }

    public boolean checkUid(int uid, int book_id) {
        boolean flag = false;
        String query = "SELECT * FROM `book_approval_request` WHERE u_id = " + uid
                + " and book_id = " + book_id;

        try {
            Statement stmt = conn.createStatement();
            flag = stmt.executeQuery(query).next();
        } catch (Exception e) {
            System.err.println("BookApproval.checkUid()" + e.getMessage());
        }

        return flag;
    }

    public void insert(int uid, int book_id) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        try {
            String sql = "INSERT INTO `book_approval_request` (u_id,book_id,approval,created_at,updated_at) values (?,?,?,?,?)";
            if (!checkUid(uid, book_id)) {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, uid);
                stmt.setInt(2, book_id);
                stmt.setString(3, "Processing");
                stmt.setString(4, date);
                stmt.setString(5, date);
                stmt.executeUpdate();
            } else
                throw new IllegalStateException("Already as id -> " + uid);
        } catch (Exception e) {
            System.err.println("BookApproval.insert()" + e.getMessage());
        }
    }

    public void delete(int uid, int book_id) {
        String sql = "DELETE FROM `book_approval_request` WHERE u_id = " + uid
                + " AND book_id = " + book_id;
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("BookApproval.delete " + e.getMessage());
        } finally {
            System.out.println("uid= " + uid +
                    " book_id= " + book_id);
        }
    }

    public void update(int uid, int book_id, String approval) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

        try {
            String sql = "UPDATE `book_approval_request` SET `approval`='" + approval
                    + "', `updated_at` ='" + date
                    + "' WHERE u_id = " + uid
                    + " AND book_id = " + book_id;

            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("BookApproval.update " + e.getMessage());
        } finally {
            System.out.println("uid= " + uid +
                    " book_id= " + book_id);
        }
    }
}
