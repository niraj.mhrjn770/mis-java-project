package project.database;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class User {
    private final Connection conn;

    public User() {
        conn = Database.getConnection();
    }

    public void insertUser(String name, String email, String phone, String address) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        try {
            String url = "src/main/resources/images/default_user.png";

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO "
                    + "user(picture,name,email, phone, address,created_at,updated_at)"
                    + " VALUES (?,?,?,?,?,?,?)");

            stmt.setString(1, url);
            stmt.setString(2, name);
            stmt.setString(3, email);
            stmt.setString(4, phone);
            stmt.setString(5, address);
            stmt.setString(6, date);
            stmt.setString(7, date);
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("User.insertUser(): " + e.getMessage());
        }
    }

    public int deleteUser(int u_id) {
        try {
            String sql = "delete from user where u_id = " + u_id;
            Statement stmt = conn.createStatement();
            return stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("User.deleteUser() => " + e.getMessage());
            return -1;
        }
    }

    public void updateUser(int u_id, String name, String email, String address, String phone) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        try {
            PreparedStatement stmt = conn
                    .prepareStatement(
                            "UPDATE user SET name = ?, email = ?, phone = ?, address = ?, updated_at=? WHERE u_id = ?");
            stmt.setString(1, name);
            stmt.setString(2, email);
            stmt.setString(3, phone);
            stmt.setString(4, address);
            stmt.setString(5, date);
            stmt.setInt(6, u_id);
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("User.updateUser()" + e.getMessage());
        }
    }

    public ResultSet getDetails() {
        try {
            String sql = "Select * from user ORDER BY updated_at DESC";
            Statement statement = conn.createStatement();
            return statement.executeQuery(sql);
        } catch (Exception e) {
            System.out.println("User.getDetails(): " + e.getMessage());
            return null;
        }
    }

    public ResultSet getExtraDetails() {
        try {
            Statement statement = conn.createStatement();
            return statement.executeQuery("SELECT * FROM `user_credential` " +
                    "INNER JOIN `user` ON user_credential.user_id = user.u_id;");
        } catch (Exception e) {
            System.err.println("UserApproval.getDetails: " + e.getMessage());
            return null;
        }
    }

    public ResultSet getDetails(int u_id) {
        try {
            String sql = "Select * from user where u_id = " + u_id +
                    " ORDER BY updated_at DESC";
            Statement statement = conn.createStatement();
            return statement.executeQuery(sql);
        } catch (Exception e) {
            System.out.println("User.getDetails(): " + e.getMessage());
            return null;
        }
    }

    public int getCount() {
        int rowCount = 0;
        try {
            String sql = "SELECT COUNT(*) FROM user";
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                rowCount = rs.getInt(1); // Get the count from the first column
            }
        } catch (Exception e) {
            System.out.println("User.getCount() -> " + e.getMessage());
        }
        return rowCount;
    }

    public String getName(int u_id) {
        String name = null;
        String sql = "Select name from user where u_id = " + u_id;
        try (Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                name = rs.getString("name");
            }
        } catch (Exception e) {
            System.out.println("User.getName()" + e.getCause());
        }

        return name;
    }
}
