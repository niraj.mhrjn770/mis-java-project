package project.database;

import java.sql.*;
import java.util.Random;

public class Admin {
    private static Admin instance;
    private final Connection conn = Database.getConnection();
    private String name, password;
    private int id;

    public static Admin getInstance() {
        if (instance == null)
            instance = new Admin();

        return instance;
    }

    public int getId() {
        try {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(
                    "SELECT * FROM admin where name = '"
                    + this.name
                    + "' and password = '"
                    + this.password
                    + "'");
            while (rs.next()) {
                this.id = rs.getInt(1);
            }
            return this.id;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return -1;
        }
    }

    public boolean setLogin(String name, String password) {
        this.name = name;
        this.password = password;

        String n = null, p = null;

        String sql = "SELECT * FROM admin WHERE name = ? AND password = ?";

        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, name);
            statement.setString(2, password);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getString("name");
                p = rs.getString("password");
            }
            if (name != null && password != null) {
                return name.equals(n) && password.equals(p);
            } else {
                return false;
            }
        } catch (Exception e) {
            System.err.println("AdminDatabase.AdminDatabase(): " + e.getMessage());
            return false;
        }
    }

    public ResultSet getDetail() {
        try {
            Statement statement = conn.createStatement();
            String sql = "SELECT * from admin_detail where admin_id=" + getId();
            return statement.executeQuery(sql);
        } catch (Exception e) {
            System.err.println("Admin.getDetail(): " + e.getMessage());
            return null;
        }
    }

    public int updateAdminDetail(String name, String email, String phone)
            throws SQLException {
        int id = new Random().nextInt(16, Integer.MAX_VALUE);
        String sql = "update admin_detail set id=?, picture=?,name=?,email=?,phone=? where admin_id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.setString(2, "src/main/resources/images/default_admin.png");
        statement.setString(3, name);
        statement.setString(4, email);
        statement.setString(5, phone);
        statement.setInt(6, getId());
        return statement.executeUpdate();
    }

    public int updateAdmin(String admin, String password) throws SQLException {
        String sql = "update admin set name=?, password=? where id=" + getId();
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, admin);
        statement.setString(2, password);
        return statement.executeUpdate();
    }

    public int insertAdminDetail(String name, String email, String phone)
            throws SQLException {
        int id = new Random().nextInt(16, Integer.MAX_VALUE);
        String sql = "insert into admin_detail values(?,?,?,?,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.setString(2, "src/main/resources/images/default_admin.png");
        statement.setString(3, name);
        statement.setString(4, email);
        statement.setString(5, phone);
        statement.setInt(6, getId());
        return statement.executeUpdate();
    }

    public boolean isNullAdminDetail() {
        try (Statement statement = conn.createStatement()) {
            String sql = "SELECT COUNT(*) FROM admin_detail where admin_id=" + getId();
            ResultSet rs = statement.executeQuery(sql);
            rs.next();
            int rowCount = rs.getInt(1);
            System.out.println("AdminDatabase.isNullAdminDetail():" + rowCount);
            if (rowCount == 0) {
                System.out.println("admin_detail is empty");
                return false;
            } else {
                System.out.println("admin_detail is not empty");
                return true;
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
    }

}
