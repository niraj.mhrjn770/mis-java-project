package project.database;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Book {
    private final Connection conn;

    public Book() {
        conn = Database.getConnection();
    }

    public ResultSet getDetails() {
        try {
            String sql = "Select * from books ORDER BY updated_at DESC";
            Statement statement = conn.createStatement();
            return statement.executeQuery(sql);
        } catch (Exception e) {
            System.out.println("Book.getDetails(): " + e.getMessage());
            return null;
        }
    }

    public int getCount() {
        int rowCount = 0;
        try {
            String sql = "SELECT COUNT(*) FROM books";
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            if (rs.next()) {
                rowCount = rs.getInt(1); // Get the count from the first column
            }
        } catch (Exception e) {
            System.out.println("Book.getCount() -> " + e.getMessage());
        }
        return rowCount;
    }

    public void updateBook(int book_id, String title, String author, String category) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        try {
            String sql = "update books set title=?, author=?, category=?, updated_at=? where book_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, title);
            stmt.setString(2, author);
            stmt.setString(3, category);
            stmt.setString(4, date);
            stmt.setInt(5, book_id);
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("Book.updateBook(): " + e.getCause());
        }
    }

    public void insertBook(String title, String author, String category) {
        String url_default = "src/main/resources/images/book.jpg";
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        try {
            String sql = "INSERT INTO `books` (`picture`,`title`, `author`, `category`,`created_at`,`updated_at`)" +
                    " VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, url_default);
            stmt.setString(2, title);
            stmt.setString(3, author);
            stmt.setString(4, category);
            stmt.setString(5, date);
            stmt.setString(6, date);
            stmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("Book.insertBook(): " + e.getCause());
        }
    }

    public void deleteBook(int book_id) {
        try {
            System.out.println(book_id);
            String sql = "delete from books where book_id = " + book_id;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println("Book.deleteBook() => " + e.getCause());
        }
    }

    public String getBookTitle(int book_id) {
        String bookTitle = null;
        String sql = "Select title from books where book_id = " + book_id;
        try (Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                bookTitle = rs.getString("title");
            }
        } catch (Exception e) {
            System.out.println("Book.getBookTitle()" + e.getMessage());
        }

        return bookTitle;
    }

    public ResultSet getDetails(int book_id) {
        try {
            String sql = "Select * from books where book_id = " + book_id
                    + " ORDER BY updated_at DESC";
            Statement statement = conn.createStatement();
            return statement.executeQuery(sql);
        } catch (Exception e) {
            System.out.println("Book.getDetails(): " + e.getMessage());
            return null;
        }
    }
}
