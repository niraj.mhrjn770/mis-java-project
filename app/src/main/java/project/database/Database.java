package project.database;

import java.sql.*;

public class Database {
    private static Connection connection;

    // private static final String HOST =
    // "jdbc:mysql://mysql-1a9cad5-niraj-007.a.aivencloud.com:12808?sslmode=require/library_data",
    // USERNAME = "avnadmin", PASSWORD = "AVNS_QgnZ6mTmUM2qLhZWZy1";

    private Database() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                // connection = DriverManager.getConnection(
                //         "jdbc:mysql://localhost:3306/library_data",
                //         "root", "");
                // connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/library_data?characterEncoding=latin1&useConfigs=maxPerformance",
                    "root","");
            } catch (ClassNotFoundException | SQLException e) {
                System.err.println("Database.getConnection(); => " + e.getMessage());
            }
        }
        return connection;
    }
}
