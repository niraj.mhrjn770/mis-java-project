package project.components;

import io.github.palexdev.materialfx.controls.MFXTextField;
import io.github.palexdev.materialfx.enums.FloatMode;

public class MainTextField extends MFXTextField {
    public MainTextField(String type) {
        super();
        // super.setWidth(16);

        try {
            this.initialize();

            switch (type.toLowerCase()) {
                case "normal":
                case "":
                    break;

                case "above":
                    this.floatModeProperty().set(FloatMode.ABOVE);
                    break;

                case "border":
                    this.floatModeProperty().set(FloatMode.BORDER);
                    break;

                case "inline":
                    this.floatModeProperty().set(FloatMode.INLINE);
                    break;

                default:
                    throw new IllegalArgumentException(type + " not found");
            }
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    private void initialize() {
        this.borderGapProperty().set(12.45);
        this.prefColumnCountProperty().set(23);
        this.prefHeightProperty().set(USE_PREF_SIZE);
        this.prefWidthProperty().set(USE_COMPUTED_SIZE);
        this.maxHeight(USE_PREF_SIZE);
        this.maxWidth(USE_COMPUTED_SIZE);
    }
}
