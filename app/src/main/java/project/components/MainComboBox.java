package project.components;

import io.github.palexdev.materialfx.controls.MFXComboBox;
import javafx.geometry.Insets;

public class MainComboBox extends MFXComboBox<String> {
    public MainComboBox() {
        super();
        super.setPadding(new Insets(10));
    }
}
