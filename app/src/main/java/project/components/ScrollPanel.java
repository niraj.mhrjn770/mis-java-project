package project.components;

import io.github.palexdev.materialfx.controls.MFXScrollPane;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;

public class ScrollPanel extends MFXScrollPane {
    public ScrollPanel() {
        super();
        super.setStyle("-fx-background-color: #fafafa;");
        this.init();
    }

    public ScrollPanel(Node node) {
        super(node);
        this.init();
    }

    private void init() {
        this.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        this.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        this.setFitToWidth(true);
        this.setFitToHeight(true);
        this.setPadding(new Insets(6, 8, 4, 8));
        this.autosize();
    }
}
