package project.components;

import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.effects.DepthLevel;
import io.github.palexdev.materialfx.enums.ButtonType;
import javafx.geometry.Insets;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

public class MainBtn extends MFXButton {
    String color, colorHex;

    public MainBtn() {
        super();
        this.init();
    }

    public MainBtn(String text) {
        super(text);
        this.init();
    }

    public MainBtn(ImageView imageView) {
        super("", imageView);
        super.setStyle("-fx-background-color:transparent;");
        super.setStyle("-fx-background-color:transparent;");
        super.setPadding(new Insets(16));
        this.setMaxSize(24, 24);
        this.setMinSize(16, 16);
        super.buttonTypeProperty().set(ButtonType.RAISED);
        super.depthLevelProperty().set(DepthLevel.LEVEL1);
        super.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
    }

    public void setBgColor(String colorHEX) {
        try {
            if (isValidHexColor(colorHEX)) {
                color = colorHEX;
                this.setStyle("-fx-background-color:" + colorHEX);
            } else
                throw new IllegalArgumentException("Error, It is not RGB Hex");
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void setTextColor(String colorHEX) {
        try {
            if (isValidHexColor(colorHEX)) {
                this.colorHex = colorHEX;
                if (color != null)
                    this.setStyle("-fx-text-fill:" + colorHEX + ";-fx-background-color:" + color);
                else
                    this.setStyle("-fx-text-fill:" + colorHEX);
            } else {
                throw new IllegalArgumentException("Error, It is not RGB Hex");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private boolean isValidHexColor(String color) {
        // Check if the string starts with "#" and is followed by exactly 6 valid
        // hexadecimal characters.
        return color.matches("^#[0-9A-Fa-f]{6}$");
    }

    private void init() {
        this.setPadding(new Insets(8, 12, 8, 12));
        this.buttonTypeProperty().set(ButtonType.RAISED);
        this.depthLevelProperty().set(DepthLevel.LEVEL2);
        this.setRippleAnimateBackground(true);
        this.setRippleAnimationSpeed(0.70);
        this.setRippleBackgroundOpacity(0.35);
        this.setRippleRadius(25);
        this.setTextAlignment(TextAlignment.CENTER);
        this.setRippleColor(Color.web("#cdcdcd"));
        this.setFocusTraversable(false);
        this.setFocused(false);
    }

    public void setNoDepthlevel() {
        this.depthLevelProperty().set(DepthLevel.LEVEL1);
        // this.buttonTypeProperty().set(ButtonType.FLAT);
    }

}
