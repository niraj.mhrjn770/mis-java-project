package project.components;

import javafx.geometry.*;
import javafx.scene.layout.*;

public class CardLayout extends VBox {
    private final String STYLE = "-fx-background-color: #fafafa;"
            + "-fx-background-insets: -1 -5 -1 -1;"
            + "-fx-background-radius: 8 8 4 4;"
            + "-fx-effect: dropshadow(gaussian, rgba(174, 174, 174, 0.70), 16, 0.08, 8, 10);";

    public CardLayout() {
        super(16);
        super.setStyle(STYLE);
        super.setPadding(new Insets(16));

        VBox.setVgrow(this, Priority.ALWAYS);
        HBox.setHgrow(this, Priority.SOMETIMES);
    }

    // private void init() {
    // this.setAlignment(Pos.);
    // }
}
