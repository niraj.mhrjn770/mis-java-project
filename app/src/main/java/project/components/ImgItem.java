package project.components;

import java.io.File;

import io.github.palexdev.materialfx.controls.*;

import javafx.scene.control.Label;
import javafx.scene.image.*;

public class ImgItem {
    private final Label LABEL;
    private final MainBtn BUTTON;
    private final MFXCircleToggleNode TOGGLE_CIRCLE;

    public ImgItem(String url) {
        String string = new File(url).toURI().toString();

        Image icon = new Image(string, false);

        LABEL = new Label("", new ImageView(icon));

        BUTTON = new MainBtn(new ImageView(icon));

        TOGGLE_CIRCLE = new MFXCircleToggleNode("", new ImageView(icon));
    }

    public Label getIcon() {
        return LABEL;
    }

    public MFXButton getBtnIcon() {
        return BUTTON;
    }

    public MFXCircleToggleNode getToggleButton() {
        return TOGGLE_CIRCLE;
    }
}
