package project.log;

import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import project.components.*;
import project.database.BookApproval;
import project.modules.ApprovalModel;

public class UserTable {
    private final BookApproval bookApproval = new BookApproval();
    private final ApprovalModel main_model;

    String uid;
    String bookId;
    String name;
    String approval;
    HBox btn_box;
    MainBtn done, cancel, remove, undo, return_book;

    public UserTable(ApprovalModel main_model, String uid, String name, String approval, String bookId) {
        this.main_model = main_model;
        this.uid = uid;
        this.name = name;
        this.bookId = bookId;
        this.approval = approval;

    }

    public String getBookId() {
        return bookId;
    }

    public HBox getBtn_box() {
        done = (MainBtn) new ImgItem("src/main/resources/images/icon/done.png").getBtnIcon();
        done.setOnAction(event -> {
            bookApproval.update(Integer.parseInt(uid),
                    Integer.parseInt(bookId),
                    "Approved");
            main_model.update();
        });

        cancel = (MainBtn) new ImgItem("src/main/resources/images/icon/cross.png").getBtnIcon();
        cancel.setOnAction(event -> {
            bookApproval.update(Integer.parseInt(uid),
                    Integer.parseInt(bookId),
                    "Not Approved");
            main_model.update();
        });

        remove = (MainBtn) new ImgItem("src/main/resources/images/icon/bin.png").getBtnIcon();
        remove.setAlignment(Pos.CENTER);
        remove.autosize();
        remove.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirmation");
            alert.setContentText("Are you sure about that?\n");
            alert.showAndWait().ifPresent(response -> {
                if (response == ButtonType.OK) {
                    bookApproval.delete(Integer.parseInt(uid),
                            Integer.parseInt(bookId));
                    main_model.update();
                } else
                    alert.close();
            });
        });

        undo = (MainBtn) new ImgItem("src/main/resources/images/icon/undo.png").getBtnIcon();
        undo.setAlignment(Pos.CENTER);
        undo.autosize();
        undo.setOnAction(event -> {
            bookApproval.update(Integer.parseInt(uid),
                    Integer.parseInt(bookId),
                    "Processing");
            main_model.update();
        });

        return_book = (MainBtn) new ImgItem("src/main/resources/images/icon/return.png").getBtnIcon();
        return_book.setAlignment(Pos.CENTER);
        return_book.autosize();
        return_book.setOnAction(event -> {
            bookApproval.update(Integer.parseInt(uid),
                    Integer.parseInt(bookId),
                    "Returned");
            main_model.update();
        });

        if (approval.equalsIgnoreCase("Approved") ||
                approval.equalsIgnoreCase("Not Approved") ||
                approval.equalsIgnoreCase("Returned")) {
            done.setDisable(true);
            cancel.setDisable(true);
            remove.setDisable(true);
            return_book.setDisable(true);
            undo.setDisable(false);
        } else {
            undo.setDisable(true);
        }

        btn_box = new HBox();
        btn_box.setAlignment(Pos.CENTER);
        btn_box.setPadding(new Insets(6));
        btn_box.setSpacing(10);
        btn_box.getChildren().addAll(done, cancel, undo, return_book, remove);
        return btn_box;
    }

    public String getName() {
        return name;
    }

    public String getApproval() {
        return approval;
    }

    public String getUid() {
        return uid;
    }
}
