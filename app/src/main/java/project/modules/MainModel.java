package project.modules;

import java.util.List;
import java.util.ArrayList;

import javafx.geometry.*;
import javafx.scene.control.Label;
import javafx.scene.layout.*;

import project.common.CommonButton;
import project.common.Styles;
import project.components.CardLayout;
import project.components.ImgItem;
import project.database.Book;
import project.database.User;

public class MainModel extends VBox {
    CommonButton.Refresh refresh;

    Book book_data;
    User user_data;

    private VBox container;

    public MainModel() {
        super(16);
        super.setPadding(new Insets(16));

        container = new VBox();
        container.setPadding(new Insets(16));

        book_data = new Book();
        user_data = new User();

        refresh = CommonButton.getInstance().new Refresh();
        refresh.setOnAction(event -> update());

        this.init();
    }

    public void update() {
        container.getChildren().clear();
        container.getChildren().add(getCards());
    }

    private HBox getCards() {
        HBox pane = new HBox(32);
        HBox.setHgrow(pane, Priority.ALWAYS);
        VBox.setVgrow(pane, Priority.SOMETIMES);
        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(8));

        List<CardLayout> card_list = new ArrayList<>();

        for (int i = 0; i < 2; i++) {
            CardLayout card = new CardLayout();
            card.setAlignment(Pos.CENTER);
            card.setMinSize(128, 128);
            card.setMaxSize(256, 256);
            card_list.add(card);
            pane.getChildren().add(card_list.get(i));
        }

        card_list.get(0).getChildren().add(new TotalBooks());
        card_list.get(1).getChildren().add(new TotalUsers());

        return pane;
    }

    private void init() {
        HBox.setHgrow(this, Priority.ALWAYS);

        Label header = new Label("Main Dashboard");
        header.setStyle(Styles.HEADER1.get());

        Label label = new Label("Summary");
        label.setStyle(Styles.HEADER2.get());

        VBox header_box = new VBox(8);
        VBox.setVgrow(header_box, Priority.ALWAYS);
        header_box.setAlignment(Pos.TOP_CENTER);
        header_box.getChildren().addAll(header);

        container.getChildren().add(getCards());

        CardLayout cardLayout = new CardLayout();
        cardLayout.setMinHeight(256);
        cardLayout.getChildren().addAll(label, refresh, container);

        this.getChildren().addAll(header_box, cardLayout);
    }

    private class TotalBooks extends HBox {
        public TotalBooks() {
            super();
            super.setPadding(new Insets(8));

            this.init();
        }

        private void init() {
            ImgItem img = new ImgItem("src/main/resources/images/icon/book.png");
            Label icon = img.getIcon();

            Label label = new Label("Total Books:");
            // label.setPadding(padding);
            label.setStyle(Styles.HEADER2.get());

            Label count = new Label("" + book_data.getCount());
            // count.setPadding(padding);
            count.setStyle(Styles.HEADER1.get());

            VBox box = new VBox(16);
            VBox.setVgrow(box, Priority.ALWAYS);
            box.setPadding(new Insets(8, 16, 8, 16));
            box.setAlignment(Pos.CENTER);
            box.getChildren().addAll(label, count);

            FlowPane pane = new FlowPane();
            pane.setSnapToPixel(true);
            pane.getChildren().addAll(icon, box);

            this.getChildren().add(pane);

        }
    }

    private class TotalUsers extends HBox {
        public TotalUsers() {
            super();
            super.setPadding(new Insets(8));

            HBox.setHgrow(this, Priority.ALWAYS);

            this.init();
        }

        private void init() {
            ImgItem img = new ImgItem("src/main/resources/images/icon/reading-book.png");
            Label icon = img.getIcon();

            Label label = new Label("Total Users:");
            label.setStyle(Styles.HEADER2.get());

            Label count = new Label("" + user_data.getCount());
            count.setStyle(Styles.HEADER1.get());

            VBox box = new VBox(16);
            VBox.setVgrow(box, Priority.ALWAYS);
            box.setPadding(new Insets(8, 16, 8, 16));
            box.setAlignment(Pos.CENTER);
            box.getChildren().addAll(label, count);

            FlowPane pane = new FlowPane();
            pane.setSnapToPixel(true);
            pane.getChildren().addAll(icon, box);

            this.getChildren().add(pane);
        }
    }

}
