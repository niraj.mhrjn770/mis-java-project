package project.modules;

import java.util.List;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.util.ArrayList;

import javafx.geometry.*;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

import project.common.CommonButton;
import project.common.Styles;
import project.components.*;
import project.database.User;
import project.modules.submodules.UserSubModules;

public class UserModel extends VBox {
    CommonButton.Add addBtn;
    CommonButton.Refresh refreshBtn;

    private Panel panel;

    public UserModel() {
        super(16);
        super.setPadding(new Insets(8));

        addBtn = CommonButton.getInstance().new Add();
        addBtn.setText("Add User");
        addBtn.setOnAction(event -> UserSubModules.getInstance().new AddUser(this));

        refreshBtn = CommonButton.getInstance().new Refresh();
        refreshBtn.setOnAction(event -> update());

        panel = new Panel();

        this.init();
    }

    public void update() {
        panel.update();
    }

    private void init() {
        Label label = new Label("Users");
        label.setStyle(Styles.HEADER1.get());

        VBox header_box = new VBox(16);
        VBox.setVgrow(header_box, Priority.ALWAYS);
        header_box.setAlignment(Pos.TOP_CENTER);
        header_box.getChildren().addAll(label);

        CardLayout layout = new CardLayout();
        layout.getChildren().addAll(panel);

        this.getChildren().addAll(header_box, layout);
    }

    private List<CardLayout> getCard() {
        List<CardLayout> card_list = new ArrayList<>();
        User user_data = new User();
        try (ResultSet rs = user_data.getDetails()) {
            while (rs.next()) {
                int id = rs.getInt("u_id");

                String url = rs.getString("picture");

                // Label image = new ImgItem(url).getIcon();

                InputStream stream = new FileInputStream(url);
                Image image = new Image(stream);

                ImageView imageView = new ImageView();
                imageView.setImage(image);
                imageView.setFitHeight(128);
                imageView.setPreserveRatio(true);

                VBox image_box = new VBox();
                image_box.setAlignment(Pos.CENTER);
                image_box.getChildren().add(imageView);

                Label header = new Label(rs.getString("name"));
                header.setStyle(Styles.HEADER2.get());

                Label address = new Label(rs.getString("address"));
                Label address_label = new Label("Address:");
                address_label.setStyle("-fx-font-weight:bold");

                Label email = new Label(rs.getString("email"));
                Label email_label = new Label("Email:");
                email_label.setStyle("-fx-font-weight:bold");

                Label phone = new Label(rs.getString("phone"));
                Label phone_label = new Label("Phone no.:");
                phone_label.setStyle("-fx-font-weight:bold");

                CommonButton.Update updateBtn = CommonButton.getInstance().new Update();
                updateBtn.setText("Update User's Details");
                updateBtn.setImg("src/main/resources/images/icon/update_user.png");
                updateBtn.setOnAction(event -> UserSubModules.getInstance().new UpdateUser(this, id));

                CommonButton.Delete delBtn = CommonButton.getInstance().new Delete();
                delBtn.setOnAction(event -> UserSubModules.getInstance().new DeleteUser(this, id));

                VBox pane = new VBox(16);
                pane.setPadding(new Insets(8));
                pane.setAlignment(Pos.TOP_CENTER);
                pane.getChildren().addAll(header, image_box,
                        new HBox(8, email_label, email),
                        new HBox(8, address_label, address),
                        new HBox(8, phone_label, phone),
                        new HBox(16, updateBtn, delBtn));

                CardLayout card = new CardLayout();
                card.setPadding(new Insets(8, 16, 8, 16));
                card.setSpacing(8);
                card.getChildren().add(pane);
                card.autosize();

                card_list.add(card);
            }
        } catch (Exception e) {
            System.out.println("BookModel.getBooks():" + e.getMessage());
        }
        return card_list;
    }

    private class Panel extends VBox {
        FlowPane pane;
        ScrollPanel scrollPanel;

        public Panel() {
            super(16);
            super.setAlignment(Pos.CENTER);
            super.setPadding(new Insets(8));

            pane = new FlowPane();
            pane.setAlignment(Pos.TOP_CENTER);
            pane.setPadding(new Insets(12, 8, 16, 8));
            pane.setHgap(32);
            pane.setVgap(32);

            scrollPanel = new ScrollPanel();

            this.init();
        }

        protected void update() {
            // Flow control update panel
            pane.getChildren().clear();
            pane.getChildren().addAll(getCard());
        }

        private void init() {
            HBox hbox = new HBox(8);
            hbox.setAlignment(Pos.BASELINE_LEFT);
            hbox.getChildren().addAll(addBtn, refreshBtn);

            // Flow control updater
            this.update();

            // Scroll Bar for Flow control
            scrollPanel.setMinViewportHeight(380);
            scrollPanel.setContent(pane);

            // Flow control's Box
            VBox vbox = new VBox(scrollPanel);
            VBox.setVgrow(vbox, Priority.ALWAYS);
            vbox.setPadding(new Insets(16, 8, 12, 8));
            vbox.setPrefWidth(USE_PREF_SIZE);
            vbox.setMinHeight(200);

            this.getChildren().addAll(hbox, vbox);
            this.autosize();
        }
    }

}
