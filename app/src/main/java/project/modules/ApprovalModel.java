package project.modules;

import javafx.collections.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;
import project.common.*;
import project.components.*;
import project.database.*;
import project.log.UserTable;

import java.sql.ResultSet;
import java.util.*;

public class ApprovalModel extends VBox {
    protected VBox main_box;

    private CommonButton.Refresh refresh;
    private BookPanel book_panel;

    public ApprovalModel() {
        super(8);
        super.setPadding(new Insets(16));

        main_box = new VBox(8);

        book_panel = new BookPanel();

        refresh = CommonButton.getInstance().new Refresh();
        refresh.setOnAction(event -> this.update());

        this.init();
    }

    private TableView<UserTable> getTable(int bookid) {
        TableColumn<UserTable, String> uidCol = new TableColumn<>("uid");
        uidCol.setCellValueFactory(new PropertyValueFactory<>("uid"));

        TableColumn<UserTable, String> bookidCol = new TableColumn<>("name");
        bookidCol.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<UserTable, String> approvalCol = new TableColumn<>("Request");
        approvalCol.setCellValueFactory(new PropertyValueFactory<>("approval"));

        TableColumn<UserTable, HBox> boxCol = new TableColumn<>();
        boxCol.setCellValueFactory(new PropertyValueFactory<>("btn_box"));

        TableView<UserTable> table = new TableView<>();

        try {
            ResultSet rs = new BookApproval().getBookApprovals(bookid);
            while (rs.next()) {
                ObservableList<UserTable> data = FXCollections.observableArrayList(
                        new UserTable(this,
                                rs.getString("u_id"),
                                rs.getString("name"),
                                rs.getString("approval"),
                                rs.getString("book_id")));
                table.getItems().addAll(data);
            }
        } catch (Exception e) {
            System.err.println("ApprovalModel.getTable()" + e.getMessage());
        } finally {
            table.setTableMenuButtonVisible(false);
            table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY_FLEX_LAST_COLUMN);
            table.getColumns().add(uidCol);
            table.getColumns().add(bookidCol);
            table.getColumns().add(approvalCol);
            table.getColumns().add(boxCol);
            table.getColumns().forEach(column -> {
                column.setMinWidth(32);
                column.setEditable(false);
                column.setStyle("-fx-alignment: CENTER");
            });
            table.setRowFactory(row -> {
                return new TableRow<UserTable>() {
                    @Override
                    protected void updateItem(UserTable item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || item.getApproval() == null) {
                            setDisable(false);
                        } else {
                            if (item.getApproval().equalsIgnoreCase("returned")) {
                                setStyle("-fx-disabled-fill: transparent;" +
                                        "-fx-disabled-base: transparent;" +
                                        "-fx-background-color: #EBEBE4;" +
                                        "-fx-text-fill: #C6C6C6;");
                            } else if (item.getApproval().equalsIgnoreCase("Approved")) {
                                setStyle("-fx-background-color:" + Styles.SUCCESS_ALT_COLOR.get()
                                        + "; -fx-text-fill: #FFF;");
                            } else if (item.getApproval().equalsIgnoreCase("Not Approved")) {
                                setStyle("-fx-background-color: " + Styles.DANGER_COLOR.get()
                                        + "; -fx-text-fill: #FFF;");
                            } else
                                setStyle("");
                        }
                    }
                };
            });
            table.setSelectionModel(null);
            table.autosize();
        }

        return table;
    }

    private List<CardLayout> getBookCards() {
        List<CardLayout> cards = new ArrayList<>();

        try {
            ResultSet rs = new Book().getDetails();
            while (rs.next()) {
                int id = rs.getInt("book_id");

                Label title = new Label(rs.getString("title"));
                title.setStyle(Styles.HEADER2.get());
                title.setTextAlignment(TextAlignment.JUSTIFY);
                title.setWrapText(true);

                VBox titleBox = new VBox(8, title);
                titleBox.setPadding(new Insets(8));
                titleBox.setAlignment(Pos.TOP_CENTER);

                VBox pane = new VBox(8);
                pane.setAlignment(Pos.CENTER);
                pane.setPadding(new Insets(8, 4, 2, 4));
                pane.getChildren().add(getTable(id));

                CardLayout cardLayout = new CardLayout();
                cardLayout.setMinSize(256, 256);
                cardLayout.setPrefSize(512, 400);
                cardLayout.setSpacing(8);
                cardLayout.setAlignment(Pos.TOP_CENTER);
                cardLayout.getChildren().addAll(titleBox, pane);
                cardLayout.autosize();

                cards.add(cardLayout);
            }
        } catch (Exception e) {
            System.err.println("ApprovalModel.getBookCards()" + e.getMessage());
        }

        return cards;
    }

    public void update() {
        book_panel.update();
        book_panel.fetch();
    }

    private void init() {
        Label label = new Label("Approval Dashboard");
        label.setStyle(Styles.HEADER1.get());

        main_box.getChildren().clear();
        main_box.getChildren().add(book_panel);

        VBox vbox = new VBox(10);
        vbox.setAlignment(Pos.CENTER);
        vbox.setPadding(new Insets(8));
        vbox.getChildren().addAll(label, new HBox(8, refresh), main_box);

        this.getChildren().add(vbox);
    }

    private class BookPanel extends VBox {
        MainComboBox userBox, bookBox;
        List<String> user_list, book_list;
        private ScrollPanel scrollPanel;
        private FlowPane flowLayout;
        private CommonButton.Add add;

        public BookPanel() {
            super(8);
            super.setAlignment(Pos.TOP_CENTER);

            user_list = new ArrayList<>();
            book_list = new ArrayList<>();

            userBox = new MainComboBox();
            bookBox = new MainComboBox();

            flowLayout = new FlowPane();
            flowLayout.setAlignment(Pos.CENTER);
            flowLayout.setPadding(new Insets(4, 8, 16, 8));
            flowLayout.setHgap(32);
            flowLayout.setVgap(32);

            scrollPanel = new ScrollPanel();
            scrollPanel.setMinViewportHeight(this.getMaxHeight());

            add = CommonButton.getInstance().new Add();
            add.setText("Set Approval");

            this.fetch();
            this.init();
        }

        protected void update() {
            flowLayout.getChildren().clear();
            flowLayout.getChildren().addAll(getBookCards());
        }

        protected void fetch() {
            userBox.getItems().clear();
            bookBox.getItems().clear();
            user_list.clear();
            book_list.clear();

            try {
                ResultSet user_result = new User().getDetails();
                ResultSet book_result = new Book().getDetails();

                String user_text = "", book_text = "";

                while (user_result.next()) {
                    user_text = user_result.getString("name")
                            + "#" + user_result.getInt("u_id");
                    user_list.add(user_text);
                }

                while (book_result.next()) {
                    book_text = book_result.getString("title")
                            + "#" + book_result.getInt("book_id");
                    book_list.add(book_text);
                }
            } catch (Exception e) {
                System.out.println("ApprovalModel.BookPanel.fetch()"
                        + e.getMessage());
            } finally {
                userBox.selectFirst();
                userBox.setFloatingText("User name");
                userBox.setAllowEdit(false);
                userBox.getItems().addAll(user_list);
                userBox.setMinWidth(126);
                userBox.setPrefWidth(512);

                bookBox.selectFirst();
                bookBox.setFloatingText("Book Name");
                bookBox.setAllowEdit(false);
                bookBox.getItems().addAll(book_list);
                bookBox.setMinWidth(126);
                bookBox.setPrefWidth(512);

                add.setOnAction(event -> {
                    try {
                        BookApproval bookApproval = new BookApproval();

                        String[] user_items = userBox.getSelectedItem().split("#");
                        String[] book_items = bookBox.getSelectedItem().split("#");

                        int book_id = Integer.parseInt(book_items[1]);
                        int user_id = Integer.parseInt(user_items[1]);

                        if (bookApproval.checkUid(user_id, book_id))
                            throw new IllegalStateException("User has already been set-up: " + user_id);

                        bookApproval.insert(user_id, book_id);
                    } catch (Exception e) {
                        System.err.println("ApprovalModel.BookPanel.fetch()" +
                                e.getMessage());

                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Failed");
                        alert.setHeaderText("Failed to insert");
                        alert.setContentText("Try another time.\n" + e.getMessage());
                        alert.showAndWait();
                    } finally {
                        this.update();
                    }
                });
            }
        }

        private VBox getBox() {
            Label header = new Label("Approving the Books (Testing)");
            header.setStyle(Styles.HEADER2.get());

            // this.fetch();
            CardLayout layout = new CardLayout();
            HBox.setHgrow(layout, Priority.ALWAYS);
            VBox.setMargin(layout, new Insets(2, 0, 16, 0));
            layout.getChildren().addAll(header, new HBox(16, userBox, bookBox),
                    new HBox(16, add));
            layout.autosize();

            VBox box = new VBox(16, layout);
            box.setAlignment(Pos.TOP_CENTER);
            box.autosize();
            return box;
        }

        private void init() {
            VBox.setVgrow(this, Priority.ALWAYS);

            this.update();

            scrollPanel.setContent(flowLayout);

            this.getChildren().addAll(getBox(), scrollPanel);
        }
    }
}
