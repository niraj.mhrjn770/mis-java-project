package project.modules;

import javafx.geometry.*;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import project.common.*;
import project.components.*;
import project.database.Book;
import project.modules.submodules.BookSubModules;

import java.io.*;
import java.sql.ResultSet;
import java.util.*;

public class BookModel extends VBox {
    CommonButton.Add addBtn;
    CommonButton.Refresh refreshBtn;

    Panel panel;

    public BookModel() {
        super(16);
        super.setPadding(new Insets(8));

        addBtn = CommonButton.getInstance().new Add();
        addBtn.setText("Add Books");
        addBtn.setOnAction(event -> BookSubModules.getInstance().new CreateBook(this));

        refreshBtn = CommonButton.getInstance().new Refresh();
        refreshBtn.setOnAction(event -> update());

        panel = new Panel();

        this.init();
    }

    private void init() {
        Label label = new Label("Book Model");
        label.setStyle(Styles.HEADER1.get());

        VBox header_box = new VBox(16);
        VBox.setVgrow(header_box, Priority.ALWAYS);
        header_box.setAlignment(Pos.TOP_CENTER);
        header_box.getChildren().addAll(label);

        CardLayout layout = new CardLayout();
        layout.getChildren().addAll(panel);

        this.getChildren().addAll(header_box, layout);
    }

    // Card layout for Books
    private List<CardLayout> getBooks() {
        List<CardLayout> card_list = new ArrayList<>();

        Book book_data = new Book();
        try (ResultSet rs = book_data.getDetails()) {
            while (rs.next()) {
                int id = rs.getInt("book_id");
                String url = rs.getString("picture");

                // Label image = new ImgItem(url).getIcon();
                InputStream stream = new FileInputStream(url);
                Image image = new Image(stream);

                ImageView imageView = new ImageView();
                imageView.setImage(image);
                imageView.setFitHeight(128);
                imageView.setPreserveRatio(true);

                VBox image_box = new VBox();
                image_box.setAlignment(Pos.CENTER);
                image_box.getChildren().add(imageView);

                Label header = new Label(rs.getString("title"));
                header.setStyle(Styles.HEADER2.get());

                Label author = new Label(rs.getString("author"));
                Label author_label = new Label("Author:");
                author_label.setStyle("-fx-font-weight:bold");

                Label category = new Label(rs.getString("category"));
                Label categories_label = new Label("Category:");
                categories_label.setStyle("-fx-font-weight:bold");

                CommonButton.Update updateBtn = CommonButton.getInstance().new Update();
                updateBtn.setText("Update Details");
                updateBtn.setImg("src/main/resources/images/icon/exchange.png");
                updateBtn.setOnAction(event -> BookSubModules.getInstance().new UpdateBook(this, id));

                CommonButton.Delete delBtn = CommonButton.getInstance().new Delete();
                delBtn.setOnAction(event -> BookSubModules.getInstance().new DeleteBook(this, id));

                VBox pane = new VBox(16);
                pane.setPadding(new Insets(8));
                pane.setAlignment(Pos.TOP_CENTER);
                pane.getChildren().addAll(header, image_box,
                        new HBox(8, author_label, author),
                        new HBox(8, categories_label, category),
                        new HBox(16, updateBtn, delBtn));

                CardLayout card = new CardLayout();
                card.setPadding(new Insets(8, 16, 8, 16));
                card.setSpacing(8);
                card.getChildren().add(pane);
                card.autosize();

                card_list.add(card);
            }
        } catch (Exception e) {
            System.out.println("BookModel.getBooks():" + e.getMessage());
        }

        return card_list;
    }

    public void update() {
        System.out.println("BookModel.update()");
        panel.update();
    }

    private class Panel extends VBox {
        FlowPane pane;
        ScrollPanel scrollPanel;

        public Panel() {
            super(16);
            super.setPadding(new Insets(8));
            super.setAlignment(Pos.CENTER);

            scrollPanel = new ScrollPanel();

            pane = new FlowPane();
            pane.setAlignment(Pos.TOP_CENTER);
            pane.setPadding(new Insets(12, 8, 16, 8));
            pane.setHgap(32);
            pane.setVgap(32);

            this.init();
        }

        public void update() {
            // Flow control update panel
            pane.getChildren().clear();
            pane.getChildren().addAll(getBooks());
        }

        private void init() {
            HBox btn_box = new HBox(8);
            btn_box.setAlignment(Pos.BASELINE_LEFT);
            btn_box.getChildren().addAll(addBtn, refreshBtn);

            // Flow control updater
            this.update();

            // Scroll Bar for Flow control
            scrollPanel.setMinViewportHeight(400);
            scrollPanel.setContent(pane);

            // Flow control's Box
            VBox vbox = new VBox(scrollPanel);
            VBox.setVgrow(vbox, Priority.ALWAYS);
            vbox.setPadding(new Insets(8, 16, 8, 16));
            // vbox.setAlignment(Pos.CENTER);
            vbox.setPrefWidth(USE_PREF_SIZE);
            vbox.setMinHeight(180);

            this.getChildren().addAll(btn_box, vbox);
            this.autosize();
        }
    }

}
