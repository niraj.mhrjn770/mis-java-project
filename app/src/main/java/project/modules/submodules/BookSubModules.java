package project.modules.submodules;

import java.sql.ResultSet;

import io.github.palexdev.materialfx.css.themes.MFXThemeManager;
import io.github.palexdev.materialfx.css.themes.Themes;

import javafx.geometry.*;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import project.common.Styles;
import project.components.*;
import project.database.Book;
import project.modules.BookModel;

public class BookSubModules {
    private static BookSubModules instance;

    protected Stage stage;

    protected BookModel parent;

    protected MainBtn done, cancel;

    Book book_data;

    private BookSubModules() {
        book_data = new Book();

        stage = new Stage();
        stage.requestFocus();
        stage.centerOnScreen();
        stage.sizeToScene();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(false);

        Label done_icon = new ImgItem("src/main/resources/images/icon/checked.png").getIcon();
        done = new MainBtn("Done");

        done.setStyle("-fx-text-fill:#FFF;"
                + "-fx-background-color:" + Styles.SUCCESS_COLOR.get());
        done.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
        done.setGraphic(done_icon);
        done.setAlignment(Pos.CENTER);
        done.setGraphicTextGap(8);

        Label cancel_icon = new ImgItem("src/main/resources/images/icon/x-mark.png").getIcon();

        cancel = new MainBtn("Cancel");
        cancel.setStyle("-fx-text-fill:#FFF;"
                + "-fx-background-color:" + Styles.DANGER_COLOR.get());
        cancel.setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
        cancel.setGraphic(cancel_icon);
        cancel.setAlignment(Pos.CENTER);
        cancel.setGraphicTextGap(8);
        cancel.setOnAction(event -> stage.close());
    }

    public static BookSubModules getInstance() {
        if (instance == null)
            instance = new BookSubModules();

        return instance;
    }

    public class DeleteBook extends BorderPane {
        private int book_id;

        public DeleteBook(BookModel parent, int book_id) {
            super();

            instance.parent = parent;
            this.book_id = book_id;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);
            stage.setTitle("Delete Book");
            stage.show();
        }

        public void close() {
            stage.close();
        }

        private void init() {
            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(getBox());

            this.setCenter(layout);
        }

        private VBox getBox() {
            done.setOnAction(e -> {
                book_data.deleteBook(book_id);
                stage.close();
                parent.update();
            });

            HBox btn_box = new HBox(16, done, cancel);
            btn_box.setAlignment(Pos.TOP_CENTER);
            btn_box.setPadding(new Insets(16, 8, 4, 8));

            Label label = new Label("Do you want to delete book?");
            label.setStyle(Styles.HEADER2.get());

            Label header = new Label(book_data.getBookTitle(book_id));
            header.setStyle(Styles.HEADER1.get());

            VBox pane = new VBox(8);
            pane.setAlignment(Pos.TOP_CENTER);
            pane.setPadding(new Insets(16));
            pane.getChildren().addAll(header, label, btn_box);
            pane.autosize();

            return pane;
        }
    }

    public class UpdateBook extends BorderPane {
        private final int book_id;

        public UpdateBook(BookModel parent, int book_id) {
            super();

            instance.parent = parent;
            this.book_id = book_id;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);

            stage.setTitle("Update books");
            stage.show();
        }

        public void close() {
            stage.close();
        }

        private void init() {
            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setSpacing(8);
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(
                    new Label("Updated " + book_id),
                    getPanel());

            this.setCenter(layout);
        }

        private VBox getPanel() {
            MainTextField title = new MainTextField("Inline");
            title.floatingTextProperty().set("Enter a Title Name");

            MainTextField author = new MainTextField("Inline");
            author.floatingTextProperty().set("Enter a Author Name");

            MainTextField category = new MainTextField("Inline");
            category.floatingTextProperty().set("Enter a Category Name");

            try (ResultSet resultSet = book_data.getDetails(book_id)) {
                while (resultSet.next()) {
                    String title_text = resultSet.getString("title");
                    title.setText(title_text);

                    String author_text = resultSet.getString("author");
                    author.setText(author_text);

                    String category_text = resultSet.getString("category");
                    category.setText(category_text);
                }
            } catch (Exception e) {
                System.out.println("BookSubModules.UpdateBook.getPanel(): " + e.getCause());
                System.exit(-1);
            }

            done.setOnAction(event -> {
                book_data.updateBook(book_id, title.getText(), author.getText(), category.getText());
                stage.close();
                parent.update();
                stage.close();
            });

            VBox panel = new VBox(16);
            panel.setPadding(new Insets(8));
            panel.requestFocus();
            panel.getChildren().addAll(title, author, category, new HBox(16, done, cancel));

            return panel;
        }
    }

    public class CreateBook extends BorderPane {
        public CreateBook(BookModel parent) {
            super();

            instance.parent = parent;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);

            stage.setTitle("Update books");
            stage.show();
        }

        private void init() {
            Label header = new Label("Add Books");
            header.setStyle(Styles.HEADER2.get());

            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setSpacing(8);
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(header, getPanel());

            this.setCenter(layout);
        }

        private VBox getPanel() {
            MainTextField title = new MainTextField("Inline");
            title.floatingTextProperty().set("Enter a Title Name");

            MainTextField author = new MainTextField("Inline");
            author.floatingTextProperty().set("Enter a Author Name");

            MainTextField category = new MainTextField("Inline");
            category.floatingTextProperty().set("Enter a Category Name");

            done.setOnAction(event -> {
                book_data.insertBook(title.getText(), author.getText(), category.getText());
                parent.update();
            });

            VBox panel = new VBox(16);
            panel.setPadding(new Insets(8));
            panel.requestFocus();
            panel.getChildren().addAll(title, author, category, new HBox(16, done, cancel));

            return panel;
        }
    }
}
