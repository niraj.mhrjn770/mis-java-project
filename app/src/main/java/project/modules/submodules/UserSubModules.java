package project.modules.submodules;

import io.github.palexdev.materialfx.css.themes.MFXThemeManager;
import io.github.palexdev.materialfx.css.themes.Themes;

import java.sql.ResultSet;

import javafx.scene.paint.Color;
import javafx.scene.Scene;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

import project.common.Styles;
import project.components.*;
import project.database.User;
import project.modules.UserModel;

public class UserSubModules {
    private static UserSubModules instance;

    protected Stage stage;
    protected UserModel parent;
    protected MainBtn done, cancel;

    User user_data;

    public static UserSubModules getInstance() {
        if (instance == null)
            instance = new UserSubModules();

        return instance;
    }

    private UserSubModules() {
        user_data = new User();

        stage = new Stage();
        stage.requestFocus();
        stage.centerOnScreen();
        stage.sizeToScene();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.setAlwaysOnTop(false);

        Label done_icon = new ImgItem("src/main/resources/images/icon/checked.png").getIcon();

        done = new MainBtn("Done");
        done.setStyle("-fx-text-fill:#FFF;"
                + "-fx-background-color:" + Styles.SUCCESS_COLOR.get());
        done.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
        done.setGraphic(done_icon);
        done.setAlignment(Pos.CENTER);
        done.setGraphicTextGap(8);

        Label cancel_icon = new ImgItem("src/main/resources/images/icon/x-mark.png").getIcon();

        cancel = new MainBtn("Cancel");
        cancel.setStyle("-fx-text-fill:#FFF;"
                + "-fx-background-color:" + Styles.DANGER_COLOR.get());
        cancel.setRippleColor(Color.web(Styles.DANGER_ALT_COLOR.get()));
        cancel.setGraphic(cancel_icon);
        cancel.setAlignment(Pos.CENTER);
        cancel.setGraphicTextGap(8);
        cancel.setOnAction(event -> stage.close());
    }

    public class UpdateUser extends BorderPane {
        int u_id;

        public UpdateUser(UserModel parent, int u_id) {
            instance.parent = parent;
            this.u_id = u_id;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);

            stage.setTitle("Update User's details");
            stage.show();
        }

        private void init() {
            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setSpacing(8);
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(
                    new Label("Updated " + u_id),
                    getPanel());

            this.setCenter(layout);
        }

        private VBox getPanel() {
            MainTextField name = new MainTextField("Inline");
            name.floatingTextProperty().set("Enter a User's Name");

            MainTextField email = new MainTextField("Inline");
            email.floatingTextProperty().set("Enter an Email");

            MainTextField address = new MainTextField("Inline");
            address.floatingTextProperty().set("Enter an Address");

            MainTextField phone = new MainTextField("Inline");
            phone.floatingTextProperty().set("Enter Phone Number");

            try (ResultSet resultSet = user_data.getDetails(u_id)) {
                while (resultSet.next()) {
                    String name_text = resultSet.getString("name");
                    name.setText(name_text);

                    String email_text = resultSet.getString("email");
                    email.setText(email_text);

                    String address_text = resultSet.getString("address");
                    address.setText(address_text);

                    String phone_text = resultSet.getString("phone");
                    phone.setText(phone_text);
                }
            } catch (Exception e) {
                System.out.println("BookSubModules.UpdateBook.getPanel(): " + e.getCause());
                System.exit(-1);
            }

            done.setOnAction(event -> {
                user_data.updateUser(u_id, name.getText(), email.getText(), address.getText(), phone.getText());
                parent.update();
                stage.close();
            });

            VBox panel = new VBox(16);
            panel.setPadding(new Insets(8));
            panel.requestFocus();
            panel.getChildren().addAll(name, email, address, phone, new HBox(16, done, cancel));

            return panel;
        }
    }

    public class AddUser extends BorderPane {
        public AddUser(UserModel parent) {
            super();

            instance.parent = parent;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);
            stage.setTitle("Add User");
            stage.show();
        }

        private void init() {
            Label header = new Label("Add User");
            header.setStyle(Styles.HEADER2.get());

            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setSpacing(8);
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(header, getPanel());

            this.setCenter(layout);
        }

        private VBox getPanel() {
            MainTextField name = new MainTextField("Inline");
            name.floatingTextProperty().set("Enter a User's Name");

            MainTextField email = new MainTextField("Inline");
            email.floatingTextProperty().set("Enter an Email");

            MainTextField address = new MainTextField("Inline");
            address.floatingTextProperty().set("Enter an Address");

            MainTextField phone = new MainTextField("Inline");
            phone.floatingTextProperty().set("Enter Phone Number");

            done.setOnAction(event -> {
                user_data.insertUser(name.getText(), email.getText(), address.getText(), phone.getText());
                parent.update();
            });

            VBox panel = new VBox(16);
            panel.setPadding(new Insets(8));
            panel.requestFocus();
            panel.getChildren().addAll(name, email, address, phone, new HBox(16, done, cancel));

            return panel;
        }
    }

    public class DeleteUser extends BorderPane {
        private int u_id;

        public DeleteUser(UserModel parent, int u_id) {
            super();

            instance.parent = parent;
            this.u_id = u_id;

            Scene scene = new Scene(this);
            scene.getStylesheets().clear();
            MFXThemeManager.addOn(scene, Themes.DEFAULT, Themes.LEGACY);

            this.init();

            stage.setScene(scene);
            stage.setTitle("Delete User");
            stage.show();
        }

        public void close() {
            stage.close();
        }

        private void init() {
            CardLayout layout = new CardLayout();
            BorderPane.setMargin(layout, new Insets(16, 32, 16, 32));
            layout.setPadding(new Insets(16));
            layout.getChildren().addAll(getBox());

            this.setCenter(layout);
        }

        private VBox getBox() {
            done.setOnAction(e -> {
                System.out.println(user_data.deleteUser(u_id));
                stage.close();
                parent.update();
            });

            HBox btn_box = new HBox(16, done, cancel);
            btn_box.setAlignment(Pos.TOP_CENTER);
            btn_box.setPadding(new Insets(16, 8, 4, 8));

            Label label = new Label("Do you want to delete User?");
            label.setStyle(Styles.HEADER2.get());

            Label header = new Label(user_data.getName(u_id));
            header.setStyle(Styles.HEADER1.get());

            VBox pane = new VBox(8);
            pane.setAlignment(Pos.TOP_CENTER);
            pane.setPadding(new Insets(16));
            pane.getChildren().addAll(header, label, btn_box);
            pane.autosize();

            return pane;
        }
    }
}
