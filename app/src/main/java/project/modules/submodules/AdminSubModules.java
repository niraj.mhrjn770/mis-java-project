package project.modules.submodules;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import project.common.Styles;
import project.components.ImgItem;
import project.components.MainBtn;
import project.components.MainPasswordField;
import project.components.MainTextField;
import project.database.Admin;
import project.log.Log;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminSubModules {
    private static final AdminSubModules INSTANCE = new AdminSubModules();

    private AdminSubModules() {
    }

    public synchronized static AdminSubModules getInstance() {
        return INSTANCE;
    }


    public class AdminUpdate extends VBox {
        VBox field_box, admin_box, admin_detailbox, current;
        MainBtn next;

        MainTextField textField;
        MainPasswordField passwordField;

        public AdminUpdate() {
            super(16);
            super.setPadding(new Insets(8));
            super.setAlignment(Pos.TOP_CENTER);

            this.init();
        }

        private void init() {
            next = new MainBtn("Next");
            next.setAlignment(Pos.CENTER);
            next.setGraphicTextGap(12);
            next.setGraphic(new ImgItem("src/main/resources/images/icon/next.png").getIcon());
            next.setBgColor(Styles.INFO_COLOR.get());
            next.setRippleColor(Color.web(Styles.INFO_ALT_COLOR.get()));
            next.setTextColor("#ffffff");
            next.setOnAction(event -> {
                if (current == admin_box) {
                    field_box.getChildren().clear();
                    field_box.setPrefHeight(280);
                    current = admin_detailbox;
                } else {
                    field_box.getChildren().clear();
                    field_box.setPrefHeight(225);
                    current = admin_box;
                }

                field_box.getChildren().add(current);
            });

            field_box = new VBox(8);
            field_box.setAlignment(Pos.TOP_CENTER);

            HBox toggle_box = new HBox(8);
            toggle_box.setAlignment(Pos.CENTER_RIGHT);
            toggle_box.getChildren().add(next);

            this.initAdminChange();
            this.initAdminDetailChange();

            current = admin_box;
            field_box.getChildren().clear();
            field_box.setPrefHeight(225);
            field_box.getChildren().add(current);

            this.setPadding(new Insets(16, 8, 4, 8));
            this.getChildren().addAll(toggle_box, field_box);
        }

        private void initAdminChange() {
            admin_box = new VBox(16);
            admin_box.setAlignment(Pos.TOP_CENTER);

            HBox box = new HBox(10);
            box.setAlignment(Pos.BASELINE_CENTER);

            textField = new MainTextField("inline");
            textField.setFloatingText("Enter the admin name:");

            passwordField = new MainPasswordField("inline");
            passwordField.setFloatingText("Enter the admin password:");

            Label label = new Label("Change admin name and password");
            label.setStyle("-fx-font-weight:bold");
            label.setAlignment(Pos.CENTER);

            MainBtn save = new MainBtn("Save");
            save.setBgColor(Styles.SUCCESS_COLOR.get());
            save.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
            save.setTextColor("#ffffff");
            save.setGraphic(new ImgItem("src/main/resources/images/icon/checked.png").getIcon());
            save.setGraphicTextGap(12);
            save.setOnAction(event -> {
                Alert alert = new Alert(null);
                try {
                    String admin = textField.getText(), password = passwordField.getText();
                    boolean flag = admin != null
                            && password != null
                            && !admin.isBlank()
                            && !password.isBlank();
                    if (flag) {
                        int i = Admin.getInstance().updateAdmin(admin, password);
                        if (i > -1) {
                            alert.setAlertType(Alert.AlertType.INFORMATION);
                            alert.setTitle("Success");
                            alert.setHeaderText("Success to Update");
                            alert.setContentText("Required to logout.");
                            System.out.println("Done");
                        } else {
                            alert.setAlertType(Alert.AlertType.ERROR);
                            alert.setTitle("Failed");
                            alert.setHeaderText("Failed to Update");
                            alert.setContentText("Try another time.");
                            System.out.println("Failed");
                        }
                    } else {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setTitle("Failed");
                        alert.setHeaderText("Failed to Update");
                        alert.setContentText(
                                "Either admin field is blank or password field is blank");
                        System.out.println("Blank");
                    }
                } catch (SQLException e) {
                    System.err.println(e.getMessage());
                } finally {
                    alert.show();
                }
            });

            box.getChildren().addAll(save);

            admin_box.getChildren().addAll(label, textField, passwordField, box);
        }

        private void initAdminDetailChange() {
            admin_detailbox = new VBox(16);
            admin_detailbox.setAlignment(Pos.TOP_CENTER);

            HBox box = new HBox(10);
            box.setAlignment(Pos.BASELINE_CENTER);

            MainTextField name = new MainTextField("inline");
            MainTextField email = new MainTextField("inline");
            MainTextField phone = new MainTextField("inline");

            name.setFloatingText("Enter your real name:");
            email.setFloatingText("Enter your email:");
            phone.setFloatingText("Enter your phone:");

            Label label = new Label("Change your detail");
            label.setStyle("-fx-font-weight:bold");
            label.setAlignment(Pos.CENTER);

            MainBtn save = new MainBtn("Save");
            save.setBgColor(Styles.SUCCESS_COLOR.get());
            save.setRippleColor(Color.web(Styles.SUCCESS_ALT_COLOR.get()));
            save.setTextColor("#ffffff");
            save.setGraphic(new ImgItem("src/main/resources/images/icon/checked.png").getIcon());
            save.setGraphicTextGap(12);
            save.setOnAction(event -> {
                Alert alert = new Alert(null);
                String name_text = name.getText(), email_text = email.getText(), phone_text = phone.getText();
                try {
                    int i = -1;
                    if (!name_text.isBlank()
                            && !email_text.isBlank()
                            && !phone_text.isBlank()) {
                        if (Admin.getInstance().isNullAdminDetail() == false) {
                            i = Admin.getInstance()
                                    .insertAdminDetail(name_text, email_text, phone_text);
                            if (i > -1) {
                                alert.setAlertType(Alert.AlertType.INFORMATION);
                                alert.setTitle("Success");
                                alert.setHeaderText("Success to Insert your detail");
                                alert.setContentText("Required to logout.");
                                System.out.println("Inserted");
                            } else {
                                alert.setAlertType(Alert.AlertType.ERROR);
                                alert.setTitle("Failed");
                                alert.setHeaderText("Failed to Insert");
                                alert.setContentText("Try another time");
                                System.out.println("Failed");
                            }
                        } else {
                            i = Admin.getInstance()
                                    .updateAdminDetail(name_text, email_text, phone_text);
                            if (i > -1) {
                                alert.setAlertType(Alert.AlertType.INFORMATION);
                                alert.setTitle("Success");
                                alert.setHeaderText("Success to Updated your detail");
                                alert.setContentText("Required to logout.");
                                System.out.println("updated");
                            } else {
                                alert.setAlertType(Alert.AlertType.ERROR);
                                alert.setTitle("Failed");
                                alert.setHeaderText("Failed to Updated");
                                alert.setContentText("Try another time");
                                System.out.println("Failed");
                            }
                        }
                    } else {
                        alert.setAlertType(Alert.AlertType.WARNING);
                        alert.setTitle("Failed");
                        alert.setHeaderText("Failed to Do");
                        alert.setContentText("The Field is blank\nPlease Check it");
                        System.out.println("Blank");
                    }
                } catch (SQLException e) {
                    System.err.println(e.getMessage());
                } finally {
                    alert.show();
                }
            });

            box.getChildren().addAll(save);

            admin_detailbox.getChildren().addAll(label, name, email, phone, box);
        }
    }

    public class AdminDetail extends VBox {
        public AdminDetail() {
            super();
            super.setPrefWidth(500);
            this.setAlignment(Pos.TOP_CENTER);

            this.init();
        }

        private VBox getImageViewer() {
            VBox box = new VBox(8);

            try {
                ResultSet rs = Admin.getInstance().getDetail();
                while (rs.next()) {
                    InputStream stream = new FileInputStream(rs.getString("picture"));
                    Image image = new Image(stream);
                    ImageView imageView = new ImageView();
                    imageView.setImage(image);
                    imageView.setFitHeight(128);
                    imageView.setPreserveRatio(true);
                    box.getChildren().add(imageView);
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            } finally {
                box.setPadding(new Insets(16, 8, 4, 8));
                box.setAlignment(Pos.TOP_CENTER);
            }
            return box;
        }

        private void init() {
            HBox hbox[] = new HBox[3];
            Label label[] = new Label[3];
            Label detail_label[] = new Label[3];

            String detail[] = new String[3];
            String string[] = {"Real Name: ", "Email: ", "Phone Number: "};

            VBox box = new VBox(16);
            box.setAlignment(Pos.TOP_CENTER);
            box.setPadding(new Insets(4, 2, 8, 2));

            Label title = new Label("Admin's Details");
            title.setStyle(Styles.HEADER1.get());
            box.getChildren().add(title);

            for (int i = 0; i < hbox.length; i++) {
                hbox[i] = new HBox();
                hbox[i].setAlignment(Pos.CENTER);

                label[i] = new Label();
                label[i].setStyle("-fx-text-fill:#484b6a;-fx-font-weight:bold;");
                label[i].setText(string[i]);

                detail_label[i] = new Label();
            }

            try {
                ResultSet resultSet = Admin.getInstance().getDetail();
                while (resultSet.next()) {
                    detail = new String[]{
                            resultSet.getString("name"),
                            resultSet.getString("email"),
                            resultSet.getString("phone"),};
                }

                for (int i = 0; i < detail_label.length; i++) {
                    detail_label[i].setText(detail[i]);
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            } finally {
                box.getChildren().add(this.getImageViewer());
                for (int i = 0; i < hbox.length; i++) {
                    if (detail_label[i].getText() == null) {
                        detail = new String[]{"N/A", "N/A", "N/A"};
                        detail_label[i].setText(detail[i]);
                    }
                    hbox[i].getChildren().addAll(label[i], detail_label[i]);
                    box.getChildren().add(hbox[i]);
                }
            }
            this.getChildren().add(box);
        }
    }

    public class Activity extends VBox {

        private final Log log = Log.getInstance();
        private final TextArea textArea = new TextArea();

        public Activity() {
            super(16);

            this.init();
        }

        private void init() {
            VBox header_box = new VBox(8);

            Label label = new Label("Activity Log");
            label.setStyle(Styles.HEADER1.get());

            MainBtn clear = new MainBtn("Clear");
            clear.setGraphic(new ImgItem("src/main/resources/images/icon/trash-can.png").getIcon());
            clear.setGraphicTextGap(12);
            clear.setBgColor("#17a2b8");
            clear.setTextColor("#FFFFFF");
            clear.setRippleColor(Color.web("#AFD3E2"));
            clear.setOnAction(event -> {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Conformation");
                alert.setHeaderText("Confomation alert");
                alert.setContentText("Are you sure about that");
                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                        Alert info = new Alert(Alert.AlertType.INFORMATION);
                        info.setTitle("Information");
                        info.setHeaderText("You have cleared the Log File");
                        info.setContentText("Cleared");
                        textArea.setText("");
                        log.clear();
                        info.show();
                    } else {
                        alert.close();
                    }
                });
            });

            header_box.getChildren().addAll(label, clear);

            VBox pane = new VBox();
            VBox.setVgrow(pane, Priority.ALWAYS);
            pane.setPadding(new Insets(8));

            String string = log.getLog();

            textArea.setPrefWidth(460);
            textArea.setMinHeight(460);
            textArea.setEditable(false);
            textArea.setWrapText(true);
            textArea.setText(string);

            VBox text_box = new VBox(textArea);
            text_box.setPadding(new Insets(8));

            pane.getChildren().addAll(header_box, text_box);

            this.setSpacing(16);
            this.getChildren().add(pane);
        }
    }
}
