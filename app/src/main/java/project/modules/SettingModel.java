package project.modules;

import javafx.geometry.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import project.common.*;
import project.components.*;
import project.modules.submodules.AdminSubModules;

import java.util.*;

public class SettingModel extends VBox {
    protected CardLayout main_container;

    public SettingModel() {
        super(8);
        super.setPadding(new Insets(16));

        main_container = new CardLayout();
        main_container.setSnapToPixel(true);
        main_container.setPadding(new Insets(8));
        main_container.setSpacing(16);

        this.getChildren().add(new MainPanel());
    }

    private class MainPanel extends BorderPane {
        private List<MainBtn> btn_List;
        private AdminSubModules adminSubModules = AdminSubModules.getInstance();

        private MainBtn getBtns() {
            MainBtn btn = new MainBtn();
            btn.setRippleColor(Color.web(Styles.INFO_ALT_COLOR.get()));
            btn.setStyle("-fx-font-weight: bold;"
                    + " -fx-font-size:12px;"
                    + "-fx-text-fill:#FFF;"
                    + "-fx-background-color:" + Styles.INFO_COLOR.get());
            btn.setPadding(new Insets(8, 16, 8, 16));
            btn.autosize();
            return btn;
        }

        public MainPanel() {
            super();

            this.atTop();
            this.atCenter();
        }

        private void atTop() {
            btn_List = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                btn_List.add(getBtns());
            }

            btn_List.get(0).setText("Admin's Detail");
            btn_List.get(0).setGraphic(new ImgItem("src/main/resources/images/icon/admin_detail.png").getIcon());
            btn_List.get(0).setGraphicTextGap(12);

            btn_List.get(1).setText("Admin's Detail Update");
            btn_List.get(1).setGraphic(new ImgItem("src/main/resources/images/icon/admin_update.png").getIcon());
            btn_List.get(1).setGraphicTextGap(12);

            btn_List.get(2).setText("Log Files");
            btn_List.get(2).setGraphic(new ImgItem("src/main/resources/images/icon/file.png").getIcon());
            btn_List.get(2).setGraphicTextGap(12);

            new SwitchNode(btn_List.get(0), main_container).switchNode(adminSubModules.new AdminDetail());
            new SwitchNode(btn_List.get(1), main_container).switchNode(adminSubModules.new AdminUpdate());
            new SwitchNode(btn_List.get(2), main_container).switchNode(adminSubModules.new Activity());

            FlowPane pane = new FlowPane();
            BorderPane.setAlignment(pane, Pos.TOP_CENTER);
            pane.setPadding(new Insets(8, 6, 4, 6));
            pane.setHgap(16);
            pane.setVgap(16);
            pane.setAlignment(Pos.CENTER);
            pane.getChildren().addAll(btn_List);
            pane.autosize();

            this.setTop(pane);
        }

        private void atCenter() {
            main_container.getChildren().clear();
            main_container.getChildren().add(adminSubModules.new AdminDetail());
            main_container.autosize();

            BorderPane.setAlignment(main_container, Pos.CENTER);
            BorderPane.setMargin(main_container, new Insets(32, 8, 4, 8));

            this.setCenter(main_container);
        }
    }
}
