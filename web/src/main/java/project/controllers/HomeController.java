package project.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.*;

@Controller
public class HomeController {
    public List<String> getList() {
        return List.of("Hello", "world!");
    }

    @GetMapping("/home")
    public String getHome(Model model) {
        return "home";
    }

}
