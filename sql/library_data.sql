-- Active: 1699371527427@@127.0.0.1@3306@library_data

CREATE DATABASE library_data DEFAULT CHARACTER SET = 'utf8mb4';

USE library_data;

CREATE Table
    admin(
        id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name varchar(255) NOT NULL,
        password varchar(255) NOT NULL
    );

INSERT INTO admin VALUES (1, "admin", "admin");

INSERT INTO admin VALUES (2, "admin", "");

CREATE TABLE
    IF NOT EXISTS `books` (
        `book_id` INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        `picture` BLOB,
        `title` varchar(255),
        `author` varchar(255),
        `category` varchar(255),
        created_at text NOT NULL DEFAULT current_timestamp(),
        updated_at text NOT NULL DEFAULT current_timestamp()
    );

INSERT INTO
    `books` (
        `picture`,
        `title`,
        `author`,
        `category`
    )
VALUES (
        'src/main/resources/images/book.jpg',
        'V for Vendetta',
        'Alan Moore',
        'Comics'
    ), (
        'src/main/resources/images/book.jpg',
        'X-Men: God Loves, Man Kills',
        'Chris',
        'Comics'
    ), (
        'src/main/resources/images/book.jpg',
        'Mike Tyson : Undisputed Truth',
        'Larry Sloman, Mike Tyson',
        'Sports'
    ), (
        'src/main/resources/images/book.jpg',
        'When Breath Becomes Air',
        'Paul Kalanithi',
        'Medical'
    ), (
        'src/main/resources/images/book.jpg',
        'The Great Gatsby',
        'F. Scott Fitzgerald',
        'Fiction'
    );

CREATE Table
    user (
        u_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
        picture BLOB,
        name VARCHAR(255),
        email VARCHAR(255),
        address VARCHAR(255),
        phone TEXT,
        created_at text NOT NULL DEFAULT current_timestamp(),
        updated_at text NOT NULL DEFAULT current_timestamp()
    );

INSERT INTO
    `user` (
        `picture`,
        `name`,
        `email`,
        `address`,
        `phone`
    )
VALUES (
        "src/main/resources/images/default_user.png",
        'John Doe',
        'johndoe@example.com',
        '123 Main St, City',
        '555-123-4567'
    ), (
        "src/main/resources/images/default_user.png",
        'John Doe',
        'johndoe@example.com',
        '123 Main St, City',
        '555-123-4567'
    ), (
        "src/main/resources/images/default_user.png",
        'Jane Smith',
        'janesmith@example.com',
        '456 Elm St, Town',
        '555-987-6543'
    ), (
        "src/main/resources/images/default_user.png",
        'Bob Johnson',
        'bobjohnson@example.com',
        '789 Oak St, Village',
        '555-555-5555'
    ), (
        "src/main/resources/images/default_user.png",
        'Alice Williams',
        'alicewilliams@example.com',
        '101 Pine St, County',
        '555-111-2222'
    );

CREATE TABLE
    user_credential(
        id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
        username VARCHAR(255),
        password VARCHAR(255),
        approval BOOLEAN DEFAULT false,
        user_id INT NOT NULL,
        Foreign Key (user_id) REFERENCES `user` (u_id) ON DELETE CASCADE
    );

/*For testing*/
INSERT INTO
    user_credential (
        `username`,
        `password`,
        `user_id`
    )
VALUES (
        "Niraj Maharjan",
        "NirajMaharjan",
        21
    );

CREATE TABLE
    session (
        id int(11) PRIMARY KEY AUTO_INCREMENT,
        term BOOLEAN not NULL DEFAULT 0,
        admin_id int not null,
        Foreign Key (admin_id) REFERENCES admin(id)
    );

CREATE TABLE
    admin_detail(
        id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
        picture BLOB,
        name VARCHAR(255),
        email VARCHAR(255),
        phone VARCHAR(255),
        admin_id INT NOT NULL,
        Foreign Key (admin_id) REFERENCES admin (id) ON DELETE CASCADE
    );

INSERT INTO admin_detail
VALUES (
        15,
        null,
        "Niraj Maharjan",
        "niraj@maharjan.com",
        9813545029,
        1
    );

SELECT
    picture,
    username,
    password,
    email,
    address
FROM `user`
    INNER JOIN user_credential ON user.u_id = user_credential.user_id;

create table
    book_approval_request (
        u_id int not NULL,
        book_id int not NULL,
        approval VARCHAR (128),
        created_at text NOT NULL DEFAULT current_timestamp(),
        updated_at text NOT NULL DEFAULT current_timestamp(),
        Foreign Key (u_id) REFERENCES `user`(u_id) on delete cascade,
        Foreign Key (book_id) REFERENCES `books`(book_id) on delete cascade
    );

